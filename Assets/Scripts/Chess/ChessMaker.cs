﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessMaker : MonoBehaviour
{
	public GameObject array, win;
	int[] commands = new int[8];
	int[] functionCommands = new int[2];
	int[] bigCommands = new int[16];
	public int offset;
	public int currentOffset;
	public int functionOffset;
	public int bigCommandsOffset;
	public GameObject left, down, right, up, ff;
	public GameObject buttonRun, buttonClear, buttonLeft, buttonRight, buttonUp, buttonDown, buttonNext, buttonTryAgain, buttonListen, buttonSay, buttonF;
	public GameObject button_1, button_2, button_3, button_4;
	public const float startPoint = 4.37f;
	const float startPointFunction = -1.875f;
	public const float step = 1.25f;
	public GameObject bobo, key, overflow, filter, welldone, door;
	public const float boboStep = 1.89f;
	bool isMoved;
	bool isSmallSet;
	bool isOverflow;
	bool functionUsed;
	public bool functionLevel, functionFinished, fireLevel;
	public GameObject intro1_1, intro1_2, intro1_3, intro2_1, intro2_2, intro3_1, intro3_2, intro_4_1_honey, intro_4_1_pear, intro_4_2, intro5_1, intro5_2, intro5_3, intro6_1, intro6_2;
	int introCounter;
	public GameObject rabbitWaiting, rabbitSpeaking, horseWaiting, horseListening, forestMessage, pearText, honeyText, bee, tree;
	public bool areYouBee;
	public GameObject commandsBoard, smallCommandsBar, functionBar;
	public GameObject pear_1, pear_2, pear_3, honey_1, honey_2, honey_3, water_1, water_2, water_3, water_4, water_5, fire;
	public GameObject f;
	public GameObject stillHungry, didNotPickAll, notEnoughWater, uselessWaters, useless_4, useless_5;

	public enum Direction
	{
		LEFT,
		DOWN,
		RıGTH,
		UP,
		FF
	}

	void Start ()
	{
		array.SetActive (false);
		win.SetActive (false);
		uselessWaters.SetActive (false);
		introCounter = 0;
		offset = 0;
		currentOffset = 0;
		bigCommandsOffset = 0;
		useless_4.SetActive (false);
		useless_5.SetActive (false);
		isMoved = false;
		isSmallSet = false;
		isOverflow = false;
		functionFinished = false;
		functionLevel = false;
		fireLevel = false;
		functionUsed = false;
		DisableGamePlayButtons ();
		intro1_1.SetActive (true);
		water_4.SetActive (false);
		fire.SetActive (false);
		water_5.SetActive (false);
		intro1_2.SetActive (false);
		intro1_3.SetActive (false);
		intro2_1.SetActive (false);
		intro2_2.SetActive (false);
		intro3_1.SetActive (false);
		intro3_2.SetActive (false);
		intro5_1.SetActive (false);
		intro5_2.SetActive (false);
		intro5_3.SetActive (false);
		overflow.SetActive (false);
		buttonF.SetActive (false);
		notEnoughWater.SetActive (false);
		functionBar.SetActive (false);
		buttonTryAgain.SetActive (false);
		filter.SetActive (false);
		welldone.SetActive (false);
		rabbitWaiting.SetActive (false);
		rabbitSpeaking.SetActive (false);
		horseWaiting.SetActive (false);
		horseListening.SetActive (false);
		buttonListen.SetActive (false);
		buttonSay.SetActive (false);
		forestMessage.SetActive (false);
		pearText.SetActive (false);
		honeyText.SetActive (false);
		bee.SetActive (false);
		tree.SetActive (false);
		smallCommandsBar.SetActive (false);
		DisableLoopButtons ();
		DisableHoneysAndPearsAndWaters ();
		stillHungry.SetActive (false);
		f.SetActive (false);
		didNotPickAll.SetActive (false);
	}

	void Update ()
	{
		if (isMoved == true) {
			Debug.Log ("Run");
			if (!functionUsed) {
				if (currentOffset != offset) {
					switch (commands [currentOffset]) {
					case (int) Direction.LEFT:
						MoveLeft ();
						currentOffset++;
						break;
					case (int) Direction.RıGTH:
						MoveRight ();
						currentOffset++;
						break;
					case (int) Direction.UP:
						MoveUp ();
						currentOffset++;
						break;
					case (int) Direction.DOWN:
						MoveDown ();
						currentOffset++;
						break;
					}
				}
			} else if (currentOffset != bigCommandsOffset) {
				switch (bigCommands [currentOffset]) {
				case (int) Direction.LEFT:
					MoveLeft ();
					currentOffset++;
					break;
				case (int) Direction.RıGTH:
					MoveRight ();
					currentOffset++;
					break;
				case (int) Direction.UP:
					MoveUp ();
					currentOffset++;
					break;
				case (int) Direction.DOWN:
					MoveDown ();
					currentOffset++;
					break;
				}
			}
			// Overflow
			if (bobo.transform.position.y < -2f || bobo.transform.position.y > 4f || bobo.transform.position.x < -6f || bobo.transform.position.x > 0f) {
				Debug.Log ("OVERFLOW");
				isOverflow = true;
				overflow.SetActive (true);
				buttonTryAgain.SetActive (true);
				filter.SetActive (true);
				DisableGamePlayButtons ();
			}
			if (!isOverflow && FindObjectOfType<BoboBehaviour> ().level_3Completed && offset != 0 && currentOffset == offset) {
				Debug.Log (FindObjectOfType<BoboBehaviour> ().pearCounter);
				if (FindObjectOfType<BoboBehaviour> ().honeyCounter == 2 || FindObjectOfType<BoboBehaviour> ().pearCounter == 2) {
					PrepareLevel_5 ();
				} else {
					LoopError ();
				}
			}
			if (!isOverflow && functionLevel && ((currentOffset == bigCommandsOffset) || ( !functionUsed && (currentOffset == offset)))) {
				Debug.Log (FindObjectOfType<BoboBehaviour> ().waterCounter);
				if (FindObjectOfType<BoboBehaviour> ().waterCounter == 2) {
					functionLevel = false;
					PrepareLevel_6 ();
				} else {
					LoopError ();
				}
			}
		}
	}

	public void LoopError ()
	{	
		if (functionLevel) {
			didNotPickAll.SetActive (true);
		} else {
			stillHungry.SetActive (true);
		}
		filter.SetActive (true);
		buttonTryAgain.SetActive (true);
		DisableGamePlayButtons ();
	}

	public void EndOfGame(){
		uselessWaters.SetActive (false);
		useless_4.SetActive (false);
		useless_5.SetActive (false);
		StartCoroutine (WaitBeforeEnd ());
	}

	void EnablePears ()
	{
		pear_1.SetActive (true);
		pear_2.SetActive (true);
		pear_3.SetActive (true);
	}

	void EnableWaters(){
		water_1.SetActive (true);
		water_2.SetActive (true);
		water_3.SetActive (true);
	}

	void EnableHoneys ()
	{
		honey_1.SetActive (true);
		honey_2.SetActive (true);
		honey_3.SetActive (true);
	}

	void DisableHoneysAndPearsAndWaters ()
	{
		pear_1.SetActive (false);
		pear_2.SetActive (false);
		pear_3.SetActive (false);
		honey_1.SetActive (false);
		honey_2.SetActive (false);
		honey_3.SetActive (false);
		water_1.SetActive (false);
		water_2.SetActive (false);
		water_3.SetActive (false);
	}

	void WellDone ()
	{
		ClearButton ();
		filter.SetActive (true);
		welldone.SetActive (true);
		buttonNext.SetActive (true);
		DisableGamePlayButtons ();
	}

	public void PrepareLevel_2 ()
	{
		WellDone ();
		key.SetActive (false);
		door.SetActive (false);
	}

	public void PrepareLevel_3 ()
	{
		WellDone ();
		rabbitWaiting.SetActive (false);
		horseWaiting.SetActive (false);
	}

	public void PrepareLevel_4 ()
	{
		WellDone ();
		rabbitWaiting.SetActive (false);
		bee.SetActive (false);
		tree.SetActive (false);
	}

	public void PrepareLevel_5 ()
	{
		WellDone ();
	}

	public void PrepareLevel_6 (){
		WellDone ();
		DisableHoneysAndPearsAndWaters ();
		f.SetActive (false);
		buttonF.SetActive (false);
		functionBar.SetActive (false);
	}

	public void Communicating ()
	{
		buttonRun.SetActive (false);
		buttonClear.SetActive (false);
		buttonSay.SetActive (true);
		buttonListen.SetActive (true);
	}

	void DisableGamePlayButtons ()
	{
		buttonLeft.SetActive (false);
		buttonRight.SetActive (false);
		buttonUp.SetActive (false);
		buttonDown.SetActive (false);
		buttonRun.SetActive (false);
		buttonClear.SetActive (false);
		buttonSay.SetActive (false);
		buttonListen.SetActive (false);
		buttonF.SetActive (false);
	}

	void EnableGamePlayButtons ()
	{
		// ...and disable next button
		buttonLeft.SetActive (true);
		buttonRight.SetActive (true);
		buttonUp.SetActive (true);
		buttonDown.SetActive (true);
		buttonRun.SetActive (true);
		buttonClear.SetActive (true);
		buttonNext.SetActive (false);
	}

	public void ListenButton ()
	{
		if (FindObjectOfType<BoboBehaviour> ().isListening == true) {
			rabbitWaiting.SetActive (false);
			rabbitSpeaking.SetActive (true);
			if (!FindObjectOfType<BoboBehaviour> ().level_2Completed) {
				forestMessage.SetActive (true);
				StartCoroutine (Listening ());
			} else {
				honeyText.SetActive (true);
				StartCoroutine (ListeningHoney ());
			}

			FindObjectOfType<BoboBehaviour> ().isListening = false;
		}
	}

	public void SayButton ()
	{
		if (FindObjectOfType<BoboBehaviour> ().isListening == false) {
			horseWaiting.SetActive (false);
			horseListening.SetActive (true);
			forestMessage.SetActive (true);
			forestMessage.transform.position = new Vector2 (-7.15f, 3.71f);
			StartCoroutine (Saying ());
		}
	}

	public void FButton(){
		if (offset != 8) {
			commands [offset] = (int)Direction.FF;
			Instantiate (ff, PositionCalculator (), Quaternion.identity);
			offset++;
			if (!functionUsed) {
				functionUsed = true;
			}
		}
	}

	public void LeftButton ()
	{
		Debug.Log ("LEFT");
		if ((!functionLevel || (functionLevel && functionFinished)) && ((!isSmallSet && offset != 8) || (isSmallSet && offset != 2))) {
			commands [offset] = (int)Direction.LEFT;
			Instantiate (left, PositionCalculator (), Quaternion.identity);
			offset++;
			if (isSmallSet && offset == 2) {
				EnableLoopButtons ();
			}
		} else if(functionLevel && !functionFinished) {
			if (functionOffset != 2) {
				functionCommands [functionOffset] = (int)Direction.LEFT;
				Instantiate (left, PositionCalculatorForFunctionLevel (), Quaternion.identity);
				functionOffset++;
				if (functionOffset == 2) {
					functionFinished = true;
					buttonRun.SetActive (true);
					buttonF.SetActive (true);
				}
			}
		}
	}

	public void DownButton ()
	{
		Debug.Log ("DOWN");
		if ((!functionLevel || (functionLevel && functionFinished)) && ((!isSmallSet && offset != 8) || (isSmallSet && offset != 2))) {
			commands [offset] = (int)Direction.DOWN;
			Instantiate (down, PositionCalculator (), Quaternion.identity);
			down.SetActive (true);
			offset++;
			if (isSmallSet && offset == 2) {
				EnableLoopButtons ();
			}
		} else if(functionLevel && !functionFinished) {
			if (functionOffset != 2) {
				functionCommands [functionOffset] = (int)Direction.DOWN;
				Instantiate (down, PositionCalculatorForFunctionLevel (), Quaternion.identity);
				functionOffset++;
				if (functionOffset == 2) {
					functionFinished = true;
					buttonRun.SetActive (true);
					buttonF.SetActive (true);
				}
			}
		}
	}

	public void RightButton ()
	{
		Debug.Log ("RıGHT");
		if ((!functionLevel || (functionLevel && functionFinished)) && ((!isSmallSet && offset != 8) || (isSmallSet && offset != 2))) {
			commands [offset] = (int)Direction.RıGTH;
			Instantiate (right, PositionCalculator (), Quaternion.identity);
			offset++;
			if (isSmallSet && offset == 2) {
				EnableLoopButtons ();
			}
		} else if(functionLevel && !functionFinished) {
			if (functionOffset != 2) {
				functionCommands [functionOffset] = (int)Direction.RıGTH;
				Instantiate (right, PositionCalculatorForFunctionLevel (), Quaternion.identity);
				functionOffset++;
				if (functionOffset == 2) {
					functionFinished = true;
					buttonRun.SetActive (true);
					buttonF.SetActive (true);
				}
			}
		}
	}

	public void UpButton ()
	{
		Debug.Log ("UP");
		if ((!functionLevel || (functionLevel && functionFinished)) && ((!isSmallSet && offset != 8) || (isSmallSet && offset != 2))) {
			commands [offset] = (int)Direction.UP;
			Instantiate (up, PositionCalculator (), Quaternion.identity);
			offset++;
			if (isSmallSet && offset == 2) {
				EnableLoopButtons ();
			}
		} else if(functionLevel && !functionFinished) {
			if (functionOffset != 2) {
				functionCommands [functionOffset] = (int)Direction.UP;
				Instantiate (up, PositionCalculatorForFunctionLevel (), Quaternion.identity);
				functionOffset++;
				if (functionOffset == 2) {
					functionFinished = true;
					buttonRun.SetActive (true);
					buttonF.SetActive (true);
				}
			}
		}
	}

	void EnableLoopButtons ()
	{
		button_1.SetActive (true);
		button_2.SetActive (true);
		button_3.SetActive (true);
		button_4.SetActive (true);
	}

	void DisableLoopButtons ()
	{
		button_1.SetActive (false);
		button_2.SetActive (false);
		button_3.SetActive (false);
		button_4.SetActive (false);
	}

	public void TryAgainButton ()
	{
		ClearButton ();
		buttonTryAgain.SetActive (false);
		overflow.SetActive (false);
		stillHungry.SetActive (false);
		EnableGamePlayButtons ();
		if (FindObjectOfType<BoboBehaviour> ().level_3Completed) {
			buttonRun.SetActive (false);
		}
		if (functionLevel) {
			buttonF.SetActive (false);
			buttonRun.SetActive (false);
			didNotPickAll.SetActive (false);
		}
		notEnoughWater.SetActive (false);
		filter.SetActive (false);
	}

	public void Button_1 ()
	{
		isMoved = true;
	}

	public void Button_2 ()
	{
		for (int i = 0; i < 3; i += 2) {
			commands [i] = commands [0];
			commands [i + 1] = commands [1];
		}
		offset = 4;
		isMoved = true;
	}

	public void Button_3 ()
	{
		for (int i = 0; i < 5; i += 2) {
			commands [i] = commands [0];
			commands [i + 1] = commands [1];
		}
		offset = 6;
		isMoved = true;
		FindObjectOfType<BoboBehaviour> ().enoughItems = true;
	}

	public void Button_4 ()
	{
		for (int i = 0; i < 7; i += 2) {
			commands [i] = commands [0];
			commands [i + 1] = commands [1];
		}
		offset = 8;
		isMoved = true;

	}

	public void RunButton ()
	{
		Debug.Log ("RUN");
		if (functionLevel && functionUsed) {
			for (int i = 0; i < offset; i++) {
				if (commands [i] != (int)Direction.FF) {
					bigCommands [bigCommandsOffset] = commands [i];
					bigCommandsOffset++;
				} else {
					for (int j = 0; j < 2; j++) {
						bigCommands [bigCommandsOffset] = functionCommands [j];
						bigCommandsOffset++;
					}
				}
			}
		}
		isMoved = true;
	}

	public void ClearButton ()
	{
		Debug.Log ("CLEAR");
		var directions = GameObject.FindGameObjectsWithTag ("Direction");
		foreach (GameObject item in directions) {
			Destroy (item);
		}
		key.transform.parent = null;
		Vector3 scale = bobo.transform.localScale;
		scale.x = 3f;
		bobo.transform.localScale = scale;
		FindObjectOfType<BoboBehaviour> ().isKeyGrabbed = false;
		FindObjectOfType<BoboBehaviour> ().isMessageTaken = false;
		Collider2D rabbit = rabbitWaiting.GetComponent<Collider2D> ();
		rabbit.enabled = true;
		key.transform.position = new Vector2 (-2.18f, -0.2250394f);
		bobo.transform.position = new Vector2 (-6f, -2f);
		offset = 0;
		currentOffset = 0;
		functionOffset = 0;
		functionFinished = false;
		functionUsed = false;
		DisableLoopButtons ();
		FindObjectOfType<BoboBehaviour> ().pearCounter = 0;
		FindObjectOfType<BoboBehaviour> ().honeyCounter = 0;
		FindObjectOfType<BoboBehaviour> ().waterCounter = 0;
		if (FindObjectOfType<BoboBehaviour> ().level_3Completed) {
			if (areYouBee) {
				EnableHoneys ();
			} else {
				EnablePears ();
			}
		}
		if(functionLevel){
			EnableWaters ();
			buttonRun.SetActive (false);
		}
		if(fireLevel){
			water_4.SetActive (true);
			water_5.SetActive (true);
			FindObjectOfType<BoboBehaviour> ().waterCounter = 3;
			useless_4.SetActive (false);
			useless_5.SetActive (false);
		}
		isOverflow = false;
		bigCommandsOffset = 0;
		buttonF.SetActive (false);
		isMoved = false;
	}

	public void NextButton ()
	{
		introCounter++;
		// take the key
		if (introCounter == 1) {
			intro1_2.SetActive (true);
			intro1_1.SetActive (false);
		}
		// use arrows
		else if (introCounter == 2) {
			intro1_3.SetActive (true);
			intro1_2.SetActive (false);
		}
		// level 1 starts
		else if (introCounter == 3) {
			intro1_3.SetActive (false);
			EnableGamePlayButtons ();
		}
		// first job completed
		else if (introCounter == 4) {
			intro2_1.SetActive (true);
			welldone.SetActive (false);
			filter.SetActive (false);
		}
		// take message from horse
		else if (introCounter == 5) {
			intro2_1.SetActive (false);
			intro2_2.SetActive (true);
		}
		// level 2 starts
		else if (introCounter == 6) {
			intro2_2.SetActive (false);
			EnableGamePlayButtons ();
			rabbitWaiting.SetActive (true);
			horseWaiting.SetActive (true);
		}
		// journey going on
		else if (introCounter == 7) {
			intro3_1.SetActive (true);
			welldone.SetActive (false);
			filter.SetActive (false);
		}
		// I'm hungry
		else if (introCounter == 8) {
			intro3_1.SetActive (false);
			intro3_2.SetActive (true);
		}
		// level 3 starts
		else if (introCounter == 9) {
			intro3_2.SetActive (false);
			EnableGamePlayButtons ();
			rabbitWaiting.SetActive (true);
			Collider2D rabbit = rabbitWaiting.GetComponent<Collider2D> ();
			rabbit.enabled = true;
			tree.SetActive (true);
			bee.SetActive (true);
		}
		// honey or bee message
		else if (introCounter == 10) {
			welldone.SetActive (false);
			filter.SetActive (false);
			if (areYouBee) {
				intro_4_1_honey.SetActive (true);
			} else {
				intro_4_1_pear.SetActive (true);
			}
		}
		// instruction
		else if (introCounter == 11) {
			intro_4_1_honey.SetActive (false);
			intro_4_1_pear.SetActive (false);
			intro_4_2.SetActive (true);
		}
		// level 4 starts
		else if (introCounter == 12) {
			intro_4_2.SetActive (false);
			EnableGamePlayButtons ();
			buttonRun.SetActive (false);
			commandsBoard.SetActive (false);
			smallCommandsBar.SetActive (true);
			isSmallSet = true;
			if (areYouBee) {
				EnableHoneys ();	
			} else {
				EnablePears ();
			}
		}
		// bobo says 1
		else if (introCounter == 13) {
			welldone.SetActive (false);
			filter.SetActive (false);
			intro5_1.SetActive (true);
		}
		// bobo speaks 2
		else if (introCounter == 14) {
			intro5_1.SetActive (false);
			intro5_2.SetActive (true);
		}
		// bobo instruc
		else if (introCounter == 15) {
			intro5_2.SetActive (false);
			intro5_3.SetActive (true);
		} else if (introCounter == 16) {
			intro5_3.SetActive (false);
			intro_4_2.SetActive (true);
		}
		// level 5 starts
		else if(introCounter == 17){
			isSmallSet = false;
			FindObjectOfType<BoboBehaviour> ().level_3Completed = false;
			DisableHoneysAndPearsAndWaters ();
			intro_4_2.SetActive (false);
			EnableGamePlayButtons ();
			EnableWaters ();
			buttonNext.SetActive (false);
			commandsBoard.SetActive (true);
			smallCommandsBar.SetActive (false);
			functionBar.SetActive (true);
			functionLevel = true;
			buttonRun.SetActive (false);
			f.SetActive (true);
		}
		// you are for..
		else if(introCounter == 18){
			welldone.SetActive (false);
			filter.SetActive (false);
			DisableGamePlayButtons ();
			intro6_1.SetActive (true);
		}
		// inscrut
		else if(introCounter == 19){
			intro6_1.SetActive (false);
			intro6_2.SetActive (true);
		}
		// level 6 starts
		else if(introCounter == 20){
			EnableGamePlayButtons ();
			intro6_2.SetActive (false);
			water_4.SetActive (true);
			water_5.SetActive (true);
			fireLevel = true;
			fire.SetActive (true);
			FindObjectOfType<BoboBehaviour> ().waterCounter = 3;
			uselessWaters.SetActive (true);
			array.SetActive (true);
		}
	}

	Vector2 PositionCalculator ()
	{
		return new Vector2 (8, startPoint - (offset * step));
	}

	Vector2 PositionCalculatorForFunctionLevel(){
		return new Vector2 (6.2f, startPointFunction - (functionOffset * step));
	}

	public void BoboFireError(){
		notEnoughWater.SetActive (true);
		buttonTryAgain.SetActive (true);
		filter.SetActive (true);
		DisableGamePlayButtons ();
	}

	void MoveRight ()
	{
		bobo.transform.position = new Vector2 (bobo.transform.position.x + boboStep, bobo.transform.position.y);
		isMoved = false;
		StartCoroutine (WaitBobo ());
		Vector3 scale = bobo.transform.localScale;
		scale.x = 3f;
		bobo.transform.localScale = scale;
	}

	void MoveLeft ()
	{
		bobo.transform.position = new Vector2 (bobo.transform.position.x - boboStep, bobo.transform.position.y);
		isMoved = false;
		StartCoroutine (WaitBobo ());
		Vector3 scale = bobo.transform.localScale;
		scale.x = -3f;
		bobo.transform.localScale = scale;
	}

	void MoveDown ()
	{

		bobo.transform.position = new Vector2 (bobo.transform.position.x, bobo.transform.position.y - boboStep);
		isMoved = false;
		StartCoroutine (WaitBobo ());
	}

	void MoveUp ()
	{

		bobo.transform.position = new Vector2 (bobo.transform.position.x, bobo.transform.position.y + boboStep);
		isMoved = false;
		StartCoroutine (WaitBobo ());
	}

	IEnumerator WaitBobo ()
	{
		yield return new WaitForSeconds (0.5f);
		if ((offset != currentOffset) || (functionUsed && (bigCommandsOffset != currentOffset))) {
			isMoved = true;
		} else {
			var directions = GameObject.FindGameObjectsWithTag ("Direction");
			foreach (GameObject item in directions) {
				Destroy (item);
			}
			offset = 0;
			currentOffset = 0;
		}
	}

	IEnumerator Listening ()
	{
		yield return new WaitForSeconds (2f);
		forestMessage.SetActive (false);
		buttonSay.SetActive (false);
		buttonListen.SetActive (false);
		buttonClear.SetActive (true);
		buttonRun.SetActive (true);
		rabbitSpeaking.SetActive (false);
		Collider2D rabbit = rabbitWaiting.GetComponent<Collider2D> ();
		rabbit.enabled = false;
		rabbitWaiting.SetActive (true);
	}

	IEnumerator ListeningHoney ()
	{
		yield return new WaitForSeconds (2f);
		honeyText.SetActive (false);
		pearText.SetActive (true);
		StartCoroutine (ListeningPear ());
	}

	IEnumerator ListeningPear ()
	{
		yield return new WaitForSeconds (2f);
		buttonSay.SetActive (false);
		buttonListen.SetActive (false);
		buttonClear.SetActive (true);
		buttonRun.SetActive (true);
		rabbitSpeaking.SetActive (false);
		Collider2D rabbit = rabbitWaiting.GetComponent<Collider2D> ();
		rabbit.enabled = false;
		rabbitWaiting.SetActive (true);
		pearText.SetActive (false);
	}

	IEnumerator Saying ()
	{
		yield return new WaitForSeconds (2f);
		forestMessage.SetActive (false);
		buttonListen.SetActive (false);
		horseListening.SetActive (false);
		buttonSay.SetActive (false);
		Collider2D horse = horseWaiting.GetComponent<Collider2D> ();
		horse.enabled = false;
		PrepareLevel_3 ();
	}

	IEnumerator WaitBeforeEnd(){
		yield return new WaitForSeconds (1f);
		DisableGamePlayButtons ();
		win.SetActive (true);
	}
}
