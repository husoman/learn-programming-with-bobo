﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoboBehaviour : MonoBehaviour
{

	public GameObject key;
	public bool isKeyGrabbed = false;
	public bool isListening = false;
	bool level_1Completed = false;
	public bool level_2Completed = false;
	public bool level_3Completed = false;
	public bool isMessageTaken = false;
	public bool enoughItems = false;
	public int honeyCounter = 0;
	public int pearCounter = 0;
	public int waterCounter = 0;

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.gameObject.tag == "Key") {
			key.transform.parent = transform;
			isKeyGrabbed = true;
		} else if (coll.gameObject.tag == "Door" && isKeyGrabbed && !level_1Completed) {
			FindObjectOfType<ChessMaker> ().PrepareLevel_2 ();
			level_1Completed = true;
		} else if (coll.gameObject.tag == "Rabbit") {
			Debug.Log ("RABBIT");
			isListening = true;
			FindObjectOfType<ChessMaker> ().Communicating ();
			isMessageTaken = true;
		} else if (coll.gameObject.tag == "Horse" && isMessageTaken) {
			Debug.Log ("HORSE");
			FindObjectOfType<ChessMaker> ().Communicating ();
			level_2Completed = true;
			isMessageTaken = false;
		} else if (coll.gameObject.tag == "Rabbit" && level_2Completed) {
			isListening = true;
			FindObjectOfType<ChessMaker> ().Communicating ();
			isMessageTaken = true;
		} else if (coll.gameObject.tag == "Bee" && isMessageTaken) {
			Debug.Log ("BEE");
			FindObjectOfType<ChessMaker> ().areYouBee = true;
			level_3Completed = true;
			FindObjectOfType<ChessMaker> ().PrepareLevel_4 ();
		} else if (coll.gameObject.tag == "Tree" && isMessageTaken) {
			Debug.Log ("TREE");
			FindObjectOfType<ChessMaker> ().areYouBee = false;
			level_3Completed = true;
			FindObjectOfType<ChessMaker> ().PrepareLevel_4 ();
		} else if (coll.gameObject.tag == "Honey") {
			coll.gameObject.SetActive (false);
			honeyCounter++;
		} else if (coll.gameObject.tag == "Pear") {
			coll.gameObject.SetActive (false);
			pearCounter++;
		}
		else if(coll.gameObject.tag == "Water"){
			coll.gameObject.SetActive (false);
			waterCounter++;
			if(FindObjectOfType<ChessMaker>().fireLevel){
				if(waterCounter == 4){
					FindObjectOfType<ChessMaker> ().useless_4.SetActive (true);
				}
				else if(waterCounter == 5){
					FindObjectOfType<ChessMaker> ().useless_5.SetActive (true);
				}
			}
		}
		else if(coll.gameObject.tag == "Fire" ){
			if (waterCounter == 5) {
				Debug.Log ("YOU WON");
				FindObjectOfType<ChessMaker> ().EndOfGame ();
				coll.gameObject.SetActive (false);
			} else {
				FindObjectOfType<ChessMaker> ().BoboFireError ();
			}
		}
	}
}
